Value Objects
===

[![build status](https://gitlab.com/guyradford/value-objects/badges/master/build.svg)](https://gitlab.com/guyradford-working/value-objects/commits/master) [![coverage report](https://gitlab.com/guyradford/value-objects/badges/master/coverage.svg)](https://gitlab.com/guyradford-working/value-objects/commits/master) 



Docker
---

Starting Docker

```bash
docker-compose up -d
```

Docker ssh
---
```bash
docker-compose exec valueobjects bash
```

Composer install
---

```bash
docker-compose exec valueobjects composer install
```

Testing
---

Running the tests.

```bash
docker-compose exec valueobjects vendor/bin/phpunit
```

Running the tests with text report code coverage.

```bash
docker-compose exec valueobjects vendor/bin/phpunit --coverage-text
```

Running the tests with html report code coverage.

```bash
docker-compose exec valueobjects vendor/bin/phpunit --coverage-html=./coverage/
```


Linting
---

```bash
docker-compose exec valueobjects vendor/bin/php-cs-fixer -v fix
```

toNative and fromNative alway use the most simple data type possible
String.
Integer.
Float (floating point numbers - also called double)
Boolean.
Url


https://github.com/ytake/valueobjects


## Change Log

### v1.7.0
* IPAddress
* IPv4Address
* IPv6Address

### v1.6.0
* Added Url

### v1.5.0
* Added Integer

### v1.4.0
* Added Iso8601DateTime

### v1.3.1
* Downgraded Uuid Library

### v1.3.0
* Added StringLiteral
* Added Hostname
