<?php
namespace GuyRadford\ValueObject\Tests;

use GuyRadford\ValueObject\Boolean;
use GuyRadford\ValueObject\Exception\InvalidArgumentException;

class BooleanTest extends \PHPUnit_Framework_TestCase
{
    public function dataProvider_fromNative_correct_values()
    {
        return [
            [true],
            [false],
        ];
    }

    /**
     * @test
     * @dataProvider dataProvider_fromNative_correct_values
     *
     * @param mixed $value
     */
    public function fromNative_correct_values($value)
    {
        $boolean = Boolean::fromNative($value);
        $this->assertEquals($value, $boolean->toNative());
    }

    public function dataProvider_fromNative_incorrect_values()
    {
        return [
            [null, 'Value "<NULL>" is not a boolean.'],
            ['false', 'Value "false" is not a boolean.'],
            ['string', 'Value "string" is not a boolean.'],
            [[1, 2, 3], 'Value "<ARRAY>" is not a boolean.'],
            [new \stdClass(), 'Value "stdClass" is not a boolean.'],
        ];
    }

    /**
     * @test
     * @dataProvider dataProvider_fromNative_incorrect_values
     */
    public function fromNative_incorrect_values($value, $message)
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage($message);
        $boolean = Boolean::fromNative($value);
    }

    public function dataProvider_fromString_correct_values()
    {
        return [
            ['true', true],
            ['True', true],
            ['TRUE', true],
            ['false', false],
            ['False', false],
            ['FALSE', false],
        ];
    }

    /**
     * @test
     * @dataProvider dataProvider_fromString_correct_values
     *
     * @param mixed $value
     */
    public function fromString_correct_values($value, $expected)
    {
        $boolean = Boolean::fromString($value);

        $this->assertEquals($expected, $boolean->toNative());
    }

    public function dataProvider_fromString_correct_alternate_values()
    {
        return [
            ['no', ['no', 'yes'], false],
            ['yes', ['no', 'yes'], true],

            ['YES', ['no', 'yes'], true],
            ['YES', ['No', 'Yes'], true],
        ];
    }

    /**
     * @test
     * @dataProvider dataProvider_fromString_correct_alternate_values
     *
     * @param mixed $value
     */
    public function fromString_correct_alternate_values($value, $allowed, $expected)
    {
        $boolean = Boolean::fromString($value, $allowed);

        $this->assertEquals($expected, $boolean->toNative());
    }

    public function dataProvider_fromString_unexpected_values()
    {
        return [
            ['yes', 'Value "yes" is not an element of the valid values: false, true'],
            ['no', 'Value "no" is not an element of the valid values: false, true'],
            ['other', 'Value "other" is not an element of the valid values: false, true'],
            [123, 'Value "123" expected to be string, type integer given.'],
            [null, 'Value "<NULL>" expected to be string, type NULL given.'],
            [new \stdClass(), 'Value "stdClass" expected to be string, type object given.'],
        ];
    }

    /**
     * @test
     * @dataProvider dataProvider_fromString_unexpected_values
     *
     * @param mixed $value
     * @param string $message
     */
    public function fromString_unexpected_values($value, $message)
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage($message);

        $boolean = Boolean::fromString($value);
    }

    public function dataProvider_toString()
    {
        return [
            [true, 'true'],
            [false, 'false']
        ];
    }

    /**
     * @test
     * @dataProvider dataProvider_toString
     *
     * @param mixed $value
     * @param string $expected
     */
    public function test_toString($value, $expected)
    {
        $boolean = Boolean::fromNative($value);
        $this->assertEquals($expected, $boolean->__toString());
        $this->assertEquals($expected, $boolean->toString());
        $this->assertEquals($expected, (string)$boolean);
    }
}
