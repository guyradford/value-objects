<?php
namespace GuyRadford\ValueObject\Tests;

use GuyRadford\ValueObject\Iso8601DateTime;

class Iso8601DateTimeTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function fromNative()
    {
        $string = '2005-08-15T15:52:01+00:00';

        $dateTime = Iso8601DateTime::fromNative($string);

        $this->assertInstanceOf(Iso8601DateTime::class, $dateTime);

        $this->assertInternalType('string', $dateTime->toNative());
    }

    /**
     * @test
     */
    public function fromDateTime()
    {
        $dateTimeExpected = new \DateTime();

        $dateTime = Iso8601DateTime::fromDateTime($dateTimeExpected);

        $this->assertInstanceOf(Iso8601DateTime::class, $dateTime);
        $this->assertEquals($dateTimeExpected, $dateTime->toDateTime());
    }

    /**
     * @test
     */
    public function now()
    {
        $dateTimeExpected = new \DateTime();
        $dateTime = Iso8601DateTime::now();
        $this->assertInstanceOf(Iso8601DateTime::class, $dateTime);
        $this->assertEquals($dateTimeExpected, $dateTime->toDateTime(), '', 1);
    }

    /**
     * @test
     */
    public function toNative()
    {
        $string = '2005-08-15T15:52:01+00:00';

        $dateTime = Iso8601DateTime::fromNative($string);

        $this->assertEquals($string, $dateTime->toNative());
    }

    /**
     * @test
     */
    public function toDateTime()
    {
        $string = '2005-08-15T15:52:01+00:00';

        $dateTime = Iso8601DateTime::fromNative($string);

        $this->assertInstanceOf(\DateTime::class, $dateTime->toDateTime());
    }

    /**
     * @test
     */
    public function test__toString()
    {
        $string = '2005-08-15T15:52:01+00:00';

        $dateTime = Iso8601DateTime::fromNative($string);

        $this->assertEquals($string, $dateTime->toNative());
        $this->assertEquals($string, $dateTime->__toString());
    }
}
