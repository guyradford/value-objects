<?php
namespace GuyRadford\ValueObject\Tests;

use GuyRadford\ValueObject\Exception\InvalidArgumentException;
use GuyRadford\ValueObject\Url;

class UrlTest extends \PHPUnit_Framework_TestCase
{
    public function dataProvider_invalid_urls()
    {
        return [
            [null],
            [true],
            [false],
            [new \stdClass()],
            [false],
            [[]],
            [0],
            ['inv@lìd'],

            ['http://'],
            ['http://.'],
            ['http://..'],
            ['http://../'],
            ['http://?'],
            ['http://??'],
            ['http://??/'],
            ['http://#'],
            ['http://##'],
            ['http://##/'],
            ['http://foo.bar?q=Spaces should be encoded'],
            ['//'],
            ['//a'],
            ['///a'],
            ['///'],
            ['http:///a'],
            ['foo.com'],
            ['rdar://1234'],
            ['h://test'],
            ['http:// shouldfail.com'],
            [':// should fail'],
            ['http://foo.bar/foo(bar)baz quux'],
            ['ftps://foo.bar/'],
//            ["http://-error-.invalid/"],
//            ["http://a.b--c.de/"],
//            ["http://-a.b.co"],
//            ["http://a.b-.co"],
//            ["http://0.0.0.0"],
//            ["http://10.1.1.0"],
//            ["http://10.1.1.255"],
//            ["http://224.1.1.1"],
//            ["http://1.1.1.1.1"],
//            ["http://123.123.123"],
//            ["http://3628126748"],

//            ["http://.www.foo.bar/"],
//            ["http://www.foo.bar./"],
//            ["http://.www.foo.bar./"],
//            ["http://10.1.1.1"],

        ];
    }

    public function dataProvider_valid_urls()
    {
        return [
            ['http://foo.com/blah_blah'],
            ['http://foo.com/blah_blah/'],
            ['http://foo.com/blah_blah_(wikipedia)'],
            ['http://foo.com/blah_blah_(wikipedia)_(again)'],
            ['http://www.example.com/wpstyle/?p=364'],
            ['https://www.example.com/foo/?bar=baz&inga=42&quux'],
            ['http://✪df.ws/123'],
            ['http://userid:password@example.com:8080'],
            ['http://userid:password@example.com:8080/'],
            ['http://userid@example.com'],
            ['http://userid@example.com/'],
            ['http://userid@example.com:8080'],
            ['http://userid@example.com:8080/'],
            ['http://userid:password@example.com'],
            ['http://userid:password@example.com/'],
            ['http://142.42.1.1/'],
            ['http://142.42.1.1:8080/'],
//            ["http://➡.ws/䨹"],
            ['http://⌘.ws'],
            ['http://⌘.ws/'],
            ['http://foo.com/blah_(wikipedia)#cite-1'],
            ['http://foo.com/blah_(wikipedia)_blah#cite-1'],
//            ["http://foo.com/unicode_(✪)_in_parens"],
            ['http://foo.com/(something)?after=parens'],
            ['http://☺.damowmow.com/'],
            ['http://code.google.com/events/#&product=browser'],
            ['http://j.mp'],
//            ["ftp://foo.bar/baz"],
            ['http://foo.bar/?q=Test%20URL-encoded%20stuff'],
//            ["http://مثال.إختبار"],
//            ["http://例子.测试"],
//            ["http://उदाहरण.परीक्षा"],
//            ["http://-.~_!$&'()*+,;=:%40:80%2f::::::@example.com"],
            ['http://1337.net'],
            ['http://a.b-c.de'],
            ['http://223.255.255.254'],
        ];
    }

    /**
     * @test
     * @dataProvider dataProvider_invalid_urls
     *
     * @param mixed $value
     */
    public function testInvalidUrl($value)
    {
        $this->expectException(InvalidArgumentException::class);
        Url::fromNative($value);
    }

    /**
     * @test
     * @dataProvider dataProvider_valid_urls
     *
     * @param mixed $value
     */
    public function valid($value)
    {
        $url = Url::fromNative($value);

        $this->assertInstanceOf(Url::class, $url);

        $this->assertInternalType('string', $url->toNative());

        $this->assertEquals($value, $url->toNative());
    }

    /**
     * @test
     */
    public function isEqual()
    {
        $testHostname = 'http://example.com';
        $hostname1 = Url::fromNative($testHostname);
        $hostname2 = Url::fromNative($testHostname);

        $this->assertTrue($hostname1->equalTo($hostname2));
    }
}
