<?php
namespace GuyRadford\ValueObject\Tests;

use GuyRadford\ValueObject\Exception\InvalidArgumentException;
use GuyRadford\ValueObject\IPv4Address;

class IPv4AddressTest extends \PHPUnit_Framework_TestCase
{
    public function dataProvider_invalid_ip_v4()
    {
        return [
            [null],
            [true],
            [false],
            [new \stdClass()],
            [false],
            [[]],
            [0],
            ['inv@lìd'],

//            ['0.42.42.42'],
            ['-12.42.42.42'],
            ['256.12.23.34'],
            ['25.256.23.34'],
            ['25.12.256.34'],
            ['25.12.23.256'],
            ['25..12.23.34'],
            ['25.12..23.34'],
            ['25.12.23..34'],
            ['12.34.45.'],
            ['12.34.45'],
            ['.12.34.45']

        ];
    }

    public function dataProvider_valid_ip_v4()
    {
        return [
            ['0.0.0.0'],
            ['255.255.255.255'],
            ['123.123.123.123']
        ];
    }

    /**
     * @test
     * @dataProvider dataProvider_invalid_ip_v4
     *
     * @param mixed $value
     */
    public function testInvalidIP($value)
    {
        $this->expectException(InvalidArgumentException::class);
        IPv4Address::fromNative($value);
    }

    /**
     * @test
     * @dataProvider dataProvider_valid_ip_v4
     *
     * @param mixed $value
     */
    public function valid($value)
    {
        $ip = IPv4Address::fromNative($value);

        $this->assertInstanceOf(IPv4Address::class, $ip);

        $this->assertInternalType('string', $ip->toNative());

        $this->assertEquals($value, $ip->toNative());
    }

    /**
     * @test
     */
    public function isEqual()
    {
        $testHostname = '12.34.56.78';
        $ip1 = IPv4Address::fromNative($testHostname);
        $ip2 = IPv4Address::fromNative($testHostname);

        $this->assertTrue($ip1->equalTo($ip2));
    }
}
