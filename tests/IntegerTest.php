<?php
namespace GuyRadford\ValueObject\Tests;

use GuyRadford\ValueObject\Exception\InvalidArgumentException;
use GuyRadford\ValueObject\Integer;

class IntegerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function fromNative()
    {
        $value = 123;

        $integer = Integer::fromNative($value);

        $this->assertInstanceOf(Integer::class, $integer);
    }

    /**
     * @test
     */
    public function toNative()
    {
        $value = 123;
        $integer = Integer::fromNative($value);
        $this->assertEquals($value, $integer->toNative());
        $this->assertInternalType('integer', $integer->toNative());
    }
    /**
     * @test
     */
    public function test__toString()
    {
        $value = 123;
        $integer = Integer::fromNative($value);
        $this->assertEquals("$value", $integer->__toString());
    }

    public function dataProvider_fromString()
    {
        return [
          [0, 0],
          [123, 123],
          ['0', 0],
          ['1', 1],
          ['123', 123],
          ['-1', -1],
          ['-345', -345],
          ['000', InvalidArgumentException::class],
          ['12.3', InvalidArgumentException::class],
          ['12.6', InvalidArgumentException::class],
          ['-12.6', InvalidArgumentException::class],
          ['One', InvalidArgumentException::class],
          ['!@£', InvalidArgumentException::class],
          [' 12', InvalidArgumentException::class],
          [' -12', InvalidArgumentException::class],
          ['24 ', InvalidArgumentException::class],
          ['-24 ', InvalidArgumentException::class],

        ];
    }

    /**
     * @test
     * @dataProvider dataProvider_fromString
     *
     * @param string $value
     * @param mixed $expected
     */
    public function fromString($value, $expected)
    {
        if ($expected === InvalidArgumentException::class) {
            $this->expectException(InvalidArgumentException::class);
        }
        $integer = Integer::fromString($value);
        if ($expected !== InvalidArgumentException::class) {
            $this->assertEquals($expected, $integer->toNative());
        }
    }

    public function dataProvider_fromFloat()
    {
        return [
            [0.0, 0],
            [12.3, 12],
            [-23.4, -23],
            [89.0, 89],

            ['12.3', InvalidArgumentException::class],
            ['-89.0', InvalidArgumentException::class],
            ['string', InvalidArgumentException::class],
            [null, InvalidArgumentException::class],
            [true, InvalidArgumentException::class],
            [[], InvalidArgumentException::class],
            [new \stdClass(), InvalidArgumentException::class],

        ];
    }

    /**
     * @test
     * @dataProvider dataProvider_fromFloat
     *
     * @param string $value
     * @param mixed $expected
     */
    public function fromFloat($value, $expected)
    {
        if ($expected === InvalidArgumentException::class) {
            $this->expectException(InvalidArgumentException::class);
        }
        $integer = Integer::fromFloat($value);
        if ($expected !== InvalidArgumentException::class) {
            $this->assertEquals($expected, $integer->toNative(), '', 0.1);
        }
    }
}
