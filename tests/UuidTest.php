<?php
namespace GuyRadford\ValueObject\Tests;

use GuyRadford\ValueObject\Uuid;

class UuidTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function generate_new()
    {
        $uuid = Uuid::generate();

        $this->assertInstanceOf(Uuid::class, $uuid);

        $this->assertInternalType('string', $uuid->toNative());
    }

    /**
     * @test
     */
    public function make_from_string()
    {
        $Uuid = \Rhumsaa\Uuid\Uuid::uuid4()->toString();

        $Uuid = Uuid::fromNative($Uuid);

        $this->assertInstanceOf(Uuid::class, $Uuid);
        $this->assertEquals($Uuid, $Uuid->toNative());
    }
}
