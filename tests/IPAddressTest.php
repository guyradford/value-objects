<?php
namespace GuyRadford\ValueObject\Tests;

use GuyRadford\ValueObject\Exception\InvalidArgumentException;
use GuyRadford\ValueObject\IPAddress;

class IPAddressTest extends \PHPUnit_Framework_TestCase
{
    public function dataProvider_invalid_ip_v4()
    {
        return [
            [null],
            [true],
            [false],
            [new \stdClass()],
            [false],
            [[]],
            [0],
            ['inv@lìd'],

//            ['0.42.42.42'],
            ['-12.42.42.42'],
            ['256.12.23.34'],
            ['25.256.23.34'],
            ['25.12.256.34'],
            ['25.12.23.256'],
            ['25..12.23.34'],
            ['25.12..23.34'],
            ['25.12.23..34'],
            ['12.34.45.'],
            ['12.34.45'],
            ['.12.34.45'],

            ['1200::AB00:1234::2552:7777:1313'],
            ['1200:0000:AB00:1234:O000:2552:7777:1313']

        ];
    }

    public function dataProvider_valid_ip_v4()
    {
        return [
            ['0.0.0.0'],
            ['255.255.255.255'],
            ['123.123.123.123'],

            ['1200:0000:AB00:1234:0000:2552:7777:1313'],
            ['21DA:D3:0:2F3B:2AA:FF:FE28:9C5A']
        ];
    }

    /**
     * @test
     * @dataProvider dataProvider_invalid_ip_v4
     *
     * @param mixed $value
     */
    public function testInvalidIP($value): void
    {
        $this->expectException(InvalidArgumentException::class);
        IPAddress::fromNative($value);
    }

    /**
     * @test
     * @dataProvider dataProvider_valid_ip_v4
     *
     * @param mixed $value
     */
    public function valid($value)
    {
        $ip = IPAddress::fromNative($value);

        $this->assertInstanceOf(IPAddress::class, $ip);

        $this->assertInternalType('string', $ip->toNative());

        $this->assertEquals($value, $ip->toNative());
    }

    /**
     * @test
     */
    public function isEqual()
    {
        $testHostname = '12.34.56.78';
        $ip1 = IPAddress::fromNative($testHostname);
        $ip2 = IPAddress::fromNative($testHostname);

        $this->assertTrue($ip1->equalTo($ip2));
    }
}
