<?php
namespace GuyRadford\ValueObject\Tests;

use GuyRadford\ValueObject\Exception\InvalidArgumentException;
use GuyRadford\ValueObject\Hostname;

class HostnameTest extends \PHPUnit_Framework_TestCase
{
    public function dataProvider_testInvalidHostname()
    {
        return [
            [null],
            [true],
            [false],
            [new \stdClass()],
            [false],
            [[]],
            [0],
            ['inv@lìd']

        ];
    }
    /**
     * @test
     * @dataProvider dataProvider_testInvalidHostname
     */
    public function testInvalidHostname($value)
    {
        $this->expectException(InvalidArgumentException::class);
        Hostname::fromNative($value);
    }

    /**
     * @test
     */
    public function valid()
    {
        $testHostname = 'example.com';
        $hostname = Hostname::fromNative($testHostname);

        $this->assertInstanceOf(Hostname::class, $hostname);

        $this->assertInternalType('string', $hostname->toNative());

        $this->assertEquals($testHostname, $hostname->toNative());
    }

    /**
     * @test
     */
    public function isEqual()
    {
        $testHostname = 'example.com';
        $hostname1 = Hostname::fromNative($testHostname);
        $hostname2 = Hostname::fromNative($testHostname);

        $this->assertTrue($hostname1->equalTo($hostname2));
    }
}
