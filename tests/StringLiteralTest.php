<?php
namespace GuyRadford\ValueObject\Tests;

use GuyRadford\ValueObject\StringLiteral;

class StringLiteralTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function fromNative()
    {
        $string = 'string to as to value object';

        $stringLiteral = StringLiteral::fromNative($string);

        $this->assertInstanceOf(StringLiteral::class, $stringLiteral);

        $this->assertInternalType('string', $stringLiteral->toNative());
    }

    /**
     * @test
     */
    public function toNative()
    {
        $string = 'string to as to value object';

        $stringLiteral = StringLiteral::fromNative($string);

        $this->assertEquals($string, $stringLiteral->toNative());
    }
    /**
     * @test
     */
    public function test__toString()
    {
        $string = 'string to as to value object';

        $stringLiteral = StringLiteral::fromNative($string);

        $this->assertEquals($string, $stringLiteral->toNative());
        $this->assertEquals($string, $stringLiteral->__toString());
    }
}
