<?php
namespace GuyRadford\ValueObject;

use GuyRadford\ValueObject\Assert\Assertion;

/**
 * Class StringLiteral
 *
 * String Value Object, used as a base for other string based Value Objects.
 *
 * @package GuyRadford\ValueObject
 *
 * @author Guy Radford<guyr@crazylime.co.uk>
 */
class Integer extends ValueObject
{
    /**
     * @var int
     */
    protected $value;

    /**
     * @param int $value
     */
    protected function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @return int
     */
    public function toNative()
    {
        return $this->value;
    }

    /**
     * Returns a object taking PHP native value(s) as argument(s).
     *
     * @return static
     */
    public static function fromNative()
    {
        $value = func_get_arg(0);

        Assertion::integer($value);

        return new static($value);
    }

    /**
     * Returns a object taking string representation of an integer.
     *
     * @param string $value
     *
     * @return static
     */
    public static function fromString($value)
    {
        Assertion::integerish($value);

        return new static(intval($value));
    }

    /**
     * Returns a object taking string representation of an integer.
     *
     * @param float $value
     * @param int $mode See http://php.net/manual/en/function.round.php for $mode options.
     *
     * @return static
     */
    public static function fromFloat($value, $mode=PHP_ROUND_HALF_UP)
    {
        Assertion::float($value);

        return new static((int)round($value, 0, $mode));
    }
}
