<?php
/**
 * Created by PhpStorm.
 * User: GuyRadford
 * Date: 17/06/2017
 * Time: 09:51
 */
namespace GuyRadford\ValueObject;

use GuyRadford\ValueObject\Assert\Assertion;

class Boolean extends ValueObject
{
    const STRING_TRUE = 'true';
    const STRING_FALSE = 'false';

    /**
     * @var bool
     */
    protected $value;

    /**
     * @param bool $value
     */
    protected function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * Returns a object taking PHP native value(s) as argument(s).
     *
     * @return static
     */
    public static function fromNative()
    {
        $value = func_get_arg(0);
        Assertion::boolean($value);

        return new static($value);
    }

    /**
     * Returns a object taking PHP native value(s) as argument(s).
     *
     * @param string $value
     * @param null|array $allowedValues
     *
     * @return static
     */
    public static function fromString($value, $allowedValues = null)
    {
        if (is_null($allowedValues)) {
            $allowedValues = [
                self::STRING_FALSE,
                self::STRING_TRUE,
            ];
        } else {
            $allowedValues = array_map('strtolower', $allowedValues);
        }

        Assertion::string($value);
        $value = strtolower($value);
        Assertion::inArray($value, $allowedValues);

        return new static((boolean) array_search($value, $allowedValues));
    }

    /**
     * Returns a native value.
     *
     * @return bool
     */
    public function toNative()
    {
        return $this->value;
    }

    /**
     * Returns a string representation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->value) {
            return self::STRING_TRUE;
        }

        return self::STRING_FALSE;
    }

    /**
     * Returns a string representation of the object
     *
     * @return string
     */
    public function toString()
    {
        return $this->__toString();
    }
}
