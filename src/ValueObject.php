<?php
namespace GuyRadford\ValueObject;

/**
 * interface ValueObjectInterface
 *
 * Base interface for a Value Object.
 *
 * @package TooBigToEmailApiBundle\ValueObject
 *
 * @author Guy Radford<guyr@crazylime.co.uk>
 */
abstract class ValueObject implements ValueObjectInterface
{
    /**
     * Compare two ValueObjectInterface and tells whether they can be considered equal
     *
     * @param  ValueObjectInterface $other
     *
     * @return bool
     */
    public function equalTo(ValueObjectInterface $other)
    {
        return ($other instanceof self)  && ($this->toNative() === $other->toNative());
    }

    /**
     * Returns a string representation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return (string)$this->toNative();
    }
}
