<?php
namespace GuyRadford\ValueObject\Exception;

class InvalidArgumentException extends \Assert\InvalidArgumentException
{
}
