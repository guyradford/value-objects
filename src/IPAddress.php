<?php
/**
 * Created by PhpStorm.
 * User: GuyRadford
 * Date: 11/06/2017
 * Time: 11:01
 */
namespace GuyRadford\ValueObject;

use GuyRadford\ValueObject\Assert\Assertion;

class IPAddress extends StringLiteral
{
    /**
     * Returns a new IPAddress
     *
     * @param string $value
     */
    public function __construct(string $value)
    {
        Assertion::ip($value);
        parent::__construct($value);
    }
}
