<?php
namespace GuyRadford\ValueObject;

use GuyRadford\ValueObject\Assert\Assertion;
use League\Uri\Schemes\Http as HttpUri;

/**
 * Class HostName
 *
 * HostName Object, used to hold the host part of a domain name.
 *
 * @package GuyRadford\ValueObject
 *
 * @author Guy Radford<guyr@crazylime.co.uk>
 */
class Url extends HttpUri implements ValueObjectInterface
{
    /**
     * @return static
     */
    public static function fromNative()
    {
        $value = func_get_arg(0);

        Assertion::url($value);

        return static::createFromString($value);
    }

    /**
     * Returns a native value.
     *
     * @return string
     */
    public function toNative()
    {
        return $this->__toString();
    }

    /**
     * Compare two ValueObjectInterface and tells whether they can be considered equal
     *
     * @param  ValueObjectInterface $other
     *
     * @return bool
     */
    public function equalTo(ValueObjectInterface $other)
    {
        return ($other instanceof self)  && ($this->toNative() === $other->toNative());
    }
}
