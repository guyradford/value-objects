<?php
namespace GuyRadford\ValueObject;

use GuyRadford\ValueObject\Assert\Assertion;

/**
 * Class StringLiteral
 *
 * String Value Object, used as a base for other string based Value Objects.
 *
 * @package GuyRadford\ValueObject
 *
 * @author Guy Radford<guyr@crazylime.co.uk>
 */
class StringLiteral extends ValueObject
{
    /**
     * @var mixed
     */
    protected $value;

    /**
     * @param string $value
     */
    protected function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function toNative()
    {
        return (string) $this->value;
    }

    /**
     * Returns a object taking PHP native value(s) as argument(s).
     *
     * @return static
     */
    public static function fromNative()
    {
        $value = func_get_arg(0);
        Assertion::string($value);

        return new static($value);
    }
}
