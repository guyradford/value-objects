<?php
namespace GuyRadford\ValueObject;

use Assert\Assertion;

/**
 * Class Uuid
 *
 * The SendId identifies a Send.
 *
 * @package TooBigToEmailApiBundle\ValueObject
 *
 * @author Guy Radford<guyr@crazylime.co.uk>
 */
class Iso8601DateTime extends ValueObject
{
    /** @var \DateTime */
    protected $value;

    /**
     * @return static
     */
    public static function now()
    {
        return new static(new \DateTime());
    }

    /**
     * @param \DateTime $dateTime
     */
    private function __construct(\DateTime $dateTime)
    {
        $this->value = $dateTime;
    }

    /**
     * @return string
     */
    public function toNative()
    {
        return $this->value->format(\DateTime::ATOM);
    }

    /**
     * @return \DateTime
     */
    public function toDateTime()
    {
        return $this->value;
    }

    /**
     * Returns an object taking PHP native value(s) as argument(s).
     *
     * @return static
     */
    public static function fromNative()
    {
        $value = func_get_arg(0);

        return new static(\DateTime::createFromFormat(\DateTime::ATOM, $value));
    }

    /**
     * Returns a object taking PHP native value(s) as argument(s).
     *
     * @return static
     */
    public static function fromDateTime()
    {
        $value = func_get_arg(0);
        Assertion::isInstanceOf($value, \DateTime::class);

        return new static($value);
    }

    /**
     * Returns a string representation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return $this->toNative();
    }
}
