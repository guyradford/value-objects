<?php
namespace GuyRadford\ValueObject\Assert;

use Assert\Assertion as BaseAssertion;
use GuyRadford\ValueObject\Exception\InvalidArgumentException;
use Zend\Validator\Hostname as HostnameValidator;

class Assertion extends BaseAssertion
{
    protected static $exceptionClass = InvalidArgumentException::class;

    /**
     * Assert that value is a php integer'ish.
     *
     * @param mixed                $value
     * @param string|callable|null $message
     * @param string|null          $propertyPath
     *
     * @return bool
     *
     * @throws \Assert\AssertionFailedException
     */
    public static function integerish($value, $message = null, $propertyPath = null) :bool
    {
        if (\is_resource($value) || \is_object($value) || (string)((int)$value) !== (string)$value || \is_bool($value) || $value === null) {
            $message = \sprintf(
                static::generateMessage($message) ?: 'Value "%s" is not an integer or a number castable to integer.',
                static::stringify($value)
            );

            throw static::createException($value, $message, static::INVALID_INTEGERISH, $propertyPath);
        }

        return true;
    }

    /**
     * Assert that value is a string hostname.
     *
     * @param mixed                $value
     * @param string|callable|null $message
     * @param string|null          $propertyPath
     *
     * @return bool
     *
     * @throws \Assert\AssertionFailedException
     */
    public static function isHostname($value, $message = null, $propertyPath = null): bool
    {
        $validator = new HostnameValidator(array('allow' => HostnameValidator::ALLOW_DNS | HostnameValidator::ALLOW_LOCAL));
        if (false === $validator->isValid($value)) {
            $message = \sprintf(
                static::generateMessage($message) ?: 'Value "%s" is not a valid hostname.',
                static::stringify($value)
            );

            throw static::createException($value, $message, static::INVALID_INTEGERISH, $propertyPath);
        }

        return true;
    }
}
