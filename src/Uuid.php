<?php
namespace GuyRadford\ValueObject;

use GuyRadford\ValueObject\Assert\Assertion;

/**
 * Class Uuid
 *
 * The SendId identifies a Send.
 *
 * @package TooBigToEmailApiBundle\ValueObject
 *
 * @author Guy Radford<guyr@crazylime.co.uk>
 */
class Uuid extends ValueObject
{
    /** @var \Rhumsaa\Uuid\Uuid */
    protected $value;
    /**
     * @return static
     */
    public static function generate()
    {
        return new static(\Rhumsaa\Uuid\Uuid::uuid4());
    }

    /**
     * @param \Rhumsaa\Uuid\Uuid $uuid
     */
    private function __construct(\Rhumsaa\Uuid\Uuid $uuid)
    {
        $this->value = $uuid;
    }

    /**
     * @return string
     */
    public function toNative()
    {
        return $this->value->toString();
    }

    /**
     * Returns a object taking PHP native value(s) as argument(s).
     *
     * @return static
     */
    public static function fromNative()
    {
        $value = func_get_arg(0);
        Assertion::string($value);
        Assertion::uuid($value);

        return new static(\Rhumsaa\Uuid\Uuid::fromString($value));
    }
}
