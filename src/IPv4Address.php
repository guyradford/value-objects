<?php
/**
 * Created by PhpStorm.
 * User: GuyRadford
 * Date: 11/06/2017
 * Time: 11:01
 */
namespace GuyRadford\ValueObject;

use GuyRadford\ValueObject\Assert\Assertion;

class IPv4Address extends IPAddress
{
    /**
     * Returns a new IPv4Address
     *
     * @param string $value
     */
    public function __construct(string $value)
    {
        Assertion::ipv4($value);
        parent::__construct($value);
    }
}
