<?php
namespace GuyRadford\ValueObject;

/**
 * interface ValueObjectInterface
 *
 * Base interface for a Value Object.
 *
 * @package GuyRadford\ValueObject
 *
 * @author Guy Radford<guyr@crazylime.co.uk>
 */
interface ValueObjectInterface
{
    /**
     * Returns a object taking PHP native value(s) as argument(s).
     *
     * @return static
     */
    public static function fromNative();

    /**
     * Returns a native value.
     *
     * @return mixed
     */
    public function toNative();

    /**
     * Compare two ValueObjectInterface and tells whether they can be considered equal
     *
     * @param  ValueObjectInterface $other
     *
     * @return bool
     */
    public function equalTo(ValueObjectInterface $other);

    /**
     * Returns a string representation of the object
     *
     * @return string
     */
    public function __toString();
}
