<?php
namespace GuyRadford\ValueObject;

use GuyRadford\ValueObject\Assert\Assertion;

/**
 * Class HostName
 *
 * HostName Object, used to hold the host part of a domain name.
 *
 * @package GuyRadford\ValueObject
 *
 * @author Guy Radford<guyr@crazylime.co.uk>
 */
class Hostname extends StringLiteral
{
    /**
     * Returns a object taking PHP native value(s) as argument(s).
     *
     * @return static
     */
    public static function fromNative()
    {
        $value = func_get_arg(0);
        Assertion::string($value);
        Assertion::isHostname($value);

        return new static($value);
    }
}
