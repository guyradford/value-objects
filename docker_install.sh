#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

apt-get update -yqq
apt-get install zip unzip git zlib1g-dev -yqq

docker-php-ext-install zip  > /dev/null

curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
composer --version

pecl install xdebug > /dev/null
docker-php-ext-enable xdebug  > /dev/null

apt-get install -y zlib1g-dev libicu-dev g++
curl -sS -o /tmp/icu.tar.gz -L http://download.icu-project.org/files/icu4c/55.1/icu4c-55_1-src.tgz && tar -zxf /tmp/icu.tar.gz -C /tmp && cd /tmp/icu/source && ./configure --prefix=/usr/local && make && make install
docker-php-ext-configure intl --with-icu-dir=/usr/local && docker-php-ext-install intl